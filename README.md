# Projet Système Bioinfo

## Script d'analyse de fichier .sam

Dans le cadre d'un projet de l'UE Système du Master Bioinformatique de l'Université de Montpellier (2021-2022), nous avons réalisé un script d'analyse de fichier .sam afin d'en retirer les reads/paires non mappés ainsi que ceux partiellement mappés. De plus, quelques statistiques concernant le fichier .sam, ses reads et ses mutations sont également fournies.

## Installation du script

Le script est portable, il n'a pas besoin d'installation particulière. Seul le fichier main.py est nécessaire.

### Environnement 

Pour la bonne exécution du script, une version récente installée de Python (>= 3.0) est nécessaire.

## Utilisation du script

L'Utilisation du script est simple :

Il suffit de lancer le script via python en lui fournissant le fichier .sam à analyser en argument.

Exemple : 

```
python3 main.py fichier_a_analyser.sam

```

Le script n'accepte qu'un seul fichier en argument et vérifiera si celui-ci est conforme au format .sam.
Si ce n'est pas le cas, un message d'erreur apparaîtra jusqu'à ce que vous fournissiez un fichier correct.

## Sorties du script

Le script produira différents fichiers de sorties (les séquences partiellement mappées/non mappées en format fasta et les statistiques en format txt) directement dans le dossier où est présent le script. 

!! Il est à remarquer que le script écrasera de précédents fichiers de sorties présents dans le dossier si ceux-là ne sont pas renommés ou déplacés !!

Un test pour déplacer ses fichiers de sortie dans un dossier portant le nom du fichier d'entrée a été réalisé mais nous avons été bloqué par la récursivité de ce système. Il est à améliorer.

## Auteurs

GANDOLFI Guillaume - Master 1 Bioinformatique, Université de Montpellier
DEFRANCE Thomas - Master 1 Bioinformatique, Université de Montpellier
