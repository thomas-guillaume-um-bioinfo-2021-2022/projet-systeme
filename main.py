import sys, os, re
from os import path

### Analyse the CIGAR = regular expression that summarise each read alignment ###
def readCigar(cigar): 
   
    ext = re.findall('\w',cigar) # split cigar 
    key=[] 
    value=[]    
    val=""

    for i in range(0,len(ext)): # For each numeric values or alpha numeric
        if (ext[i] == 'M' or ext[i] == 'I' or ext[i] == 'D' or ext[i] == 'S' or ext[i] == 'H' or ext[i] == "N" or ext[i] == 'P'or ext[i] == 'X'or ext[i] == '=') :
            key.append(ext[i])
            value.append(val)
            val = ""
        else :
            val = "" + val + ext[i]  # Else concatenate in order of arrival
    
    dico = {}
    n = 0
    for k in key:   # Dictionnary contruction in range size lists              
        if k not in dico.keys():    # for each key, insert int value
            dico[k] = int(value[n])   # if key not exist, create and add value
            n += 1
        else:
            dico[k] += int(value[n])  # inf key exist add value
            n += 1
    return dico

### Analyse the CIGAR = regular expression that summarise each read alignment ###
def percentMutation(dico):
        
    totalValue = 0 # Total number of mutations
    for v in dico :
        totalValue += dico[v]

    mutList = ['M','I','D','S','H','N','P','X','=']
    res = ""
    mutDico = {
    "M":"Alignment Match",
    "I":"Insertion",
    "D":"Deletion",
    "S":"Soft Clipping",
    "H":"Hard Clipping",
    "N":"Skipped Region",
    "P":"Padding",
    "X":"Sequence Mismatch",
    "=":"Sequence Match"
    }

    for mut in mutList : # Calculated percent of mutation if mut present in the dictionnary, else, percent of mut = 0
        if mut in dico.keys() :
            res += (str(round((dico[mut] * 100) / totalValue, 2)) + "% " + str(mutDico[mut]) +" " )
        # else :
        #     res += ("0.00" + "% ;")
    return res

# def main(argv):	

print("\n"+ "Analyzing the .sam file...")

fichier=sys.argv[1] 
if len(sys.argv) > 2 : # Vérification du nombre d'arguments
	print("Error : Too many arguments. Please select only one file .sam file.")
	sys.exit

if os.path.getsize(fichier) == 0 : # Vérification que le fichier n'est pas totalement vide
	print("Error : The file is empty.")
	sys.exit()

ext = os.path.splitext(fichier)[-1].lower()

if ext != ".sam" :
	print("Error : File not accepted. Please make sure that your file is a .sam file.")
	sys.exit()

print("Input file seems correct." + "\n")



#Ouverture du fichier sam en mode lecture

print("Initializing the program parameters...")

f=open(fichier,"r")
line=f.readlines()

# Initialisation des listes des différents attributs d'interet pour chaque read

name_list=[]
flag_att_list=[]
flag_list=[]
cigar_list=[]
seq_list=[]
mutation_list=[]

# Initialisation des listes pour l'extration des séquences d'interet (reads/paires non mappés et partiellement mappés)

seqlist_unmapped=[]
seqlist_paired_unmapped=[]
name_unmapped=[]
name_paired_unmapped=[]

seqlist_part_mapped=[]
seqlist_paired_part_mapped=[]
name_part_mapped=[]
name_paired_part_mapped=[]

# Initialisation des différents compteurs

read_count = 0
unmapped_count = 0
partially_mapped_count = 0

# Initialisation du dictionnaire de flag

flag_dictionary = {
			"duplicate": "Read is a PCR or optical duplicate.",
			"fail_quality": "Read fails quality check.",
			"not_primary": "Read is not primary.",
			"second": "Read is second read in a pair.",
			"first": "Read is first read in a pair.",
			"mate_reverse": "Mate read has been mapped on the reverse strand.",
			"reverse": "Read has been mapped on the reverse strand.",
			"mate_unmapped": "Mate read is unmapped.",
			"unmapped": "Read is unmapped.",
			"correct_reads": "Both reads are mapped properly.",
			"paired": "Read is paired."
		}

# Lancement de la boucle d'analyse
# Paramètres pour la vérification du format du header
header_count = 2
header_storage = []
header=re.compile(r'^(@)[A-Za-z][A-Za-z0-9]')
name_regex=re.compile(r'^[0-9A-Za-z!#$%&+./:;?@^_|~-][0-9A-Za-z!#$%&*+./:;=?@^_|~-]*')

# Paramètres de vérification si le read paired est déjà enregistré dans la liste

u_mate_registered = False # Pour les unmapped
p_mate_registered = False # Pour les part mapped

print("Initializing done." + "\n"+ "\n" + "Analyzing the .sam file...")

for x in line:
	if (header_count != 0):
		if re.match(header, x):
			header_count -= 1
			header_storage.append(x)
		else:
			print ("Error found in the header section.")
			break
	else:
		if re.match(name_regex, x):
			
			# Séparation des attributs et stockage dans leur variable

			name=(x.split('\t')[0])
			flag=int(x.split('\t')[1])
			seq=(x.split('\t')[9])
			cigar=(x.split('\t')[5])
			first_pair = True
			already_registered = False
			read_count += 1
			
			# Stockage des noms dans la liste de nom et modification pour le deuxieme read d'une paire
			# !! Cette partie ne fonctionne pas si les noms des reads d'une paire ont des noms différents !!

			if ( len(name_list) > 0 and name == name_list[-1]):
				name += "_2"
				name_list.append(name)
			else:
				name_list.append(name)

			# Stockage des flags et des séquences

			flag_list.append(flag)
			seq_list.append(seq)

			flagattribute=[] # Création de la liste des attributs du flag pour chaque read

			if ( flag >= 1024 ):
				flag -= 1024
				flagattribute.append("duplicate")

			if ( flag >= 512 ):
				flag -= 512
				flagattribute.append("fail_quality")

			if ( flag >= 256 ):
				flag -= 256
				flagattribute.append("not_primary")

			if ( flag >= 128 ):
				flag -= 128
				flagattribute.append("second")
				first_pair = False

			if ( flag >= 64 ):
				flag -= 64
				flagattribute.append("first")

			if ( flag >= 32 ):
				flag -= 32
				flagattribute.append("mate_reverse")

			if ( flag >= 16 ):
				flag -= 16
				flagattribute.append("reverse")

			if ( flag >= 8 ):
				flag -= 8
				flagattribute.append("mate_unmapped")
				if ((first_pair == True) or (u_mate_registered == True)): # Si c'est le premier read de la paire ou si le mate est déjà enregistré dans la liste, on rajoute le read à la liste
					seqlist_paired_unmapped.append(seq)
					name_paired_unmapped.append(name)
					already_registered = True # Si le read est mate_unmapped ET unmapped, on stocke dans une variable qu'il a déjà été enregistré dans la liste pour pas qu'il soit réenregistré dans la condition unmapped
					u_mate_registered = False

			if ( flag >= 4 ):
				flag -= 4
				flagattribute.append("unmapped")
				seqlist_unmapped.append(seq)
				name_unmapped.append(name)
				if (already_registered == False): # Si le read n'est pas déjà enregistré, alors on peut l'enregistrer
					seqlist_paired_unmapped.append(seq)
					name_paired_unmapped.append(name)
				if (first_pair == True): # Si c'est le premier read de la paire qui est le seul unmapped sur les deux, on retient que le deuxième qui n'est pas unmappedd doit être enregistré aussi
					u_mate_registered = True	
				unmapped_count += 1

			if (p_mate_registered == True):
					seqlist_paired_part_mapped.append(seq)
					name_paired_part_mapped.append(name)
					p_mate_registered = False

			if (( flag >= 2 ) or (flag <= 2 and cigar != "*")):
				if (flag >= 2):
					flag -= 2
					flagattribute.append("correct_reads")
				if cigar != "100M":
					partially_mapped_count += 1
					seqlist_part_mapped.append(seq)
					name_part_mapped.append(name)
					mutation_list.append(percentMutation(readCigar(cigar)))
					seqlist_paired_part_mapped.append(seq)
					name_paired_part_mapped.append(name)
					if (first_pair == False): # Si c'est le premier read de la paire qui est le seul part mapped sur les deux, on retient que le deuxième qui ne l'est pas doit être enregistré aussi
						seqlist_paired_part_mapped.append(seq_list[-2])
						name_paired_part_mapped.append(name_list[-2])
					else:
						p_mate_registered = True
					

			if ( flag >= 1 ):
				flag -= 1
				flagattribute.append("paired")

			flag_att_list.append(flagattribute)

print("Analizing done." + "\n" + "\n" + "Extracting the results...")


# Vérification de l'existence d'un dossier de sortie de même nom et création du dossier de sortie (Ne marche pas)

# output = os.path.splitext(fichier)[0]
# print(output)

# def output_folder() :
# 	if os.path.isdir(output) :
# 		output += "_1"
# 		return True
# 	else :
# 		return False

# while output_folder() :
# 	output_folder()
# 	print(output)

# os.mkdir(output)

# output_path = os.path.abspath(output)
# os.chdir(output_path)

# Ecriture des résultats dans des fichiers textes

with open("flag_summary.txt", "w") as flag_sum: # Résumé des attributs de chaque read
	for name in range(len(name_list)):
		flag_sum.write("Read "+name_list[name]+" is a read with these corresponding attributes :"+"\n")
		for x in flag_att_list[name]:
			flag_sum.write(flag_dictionary[x]+"\n")
		flag_sum.write("\n")

with open("mutation_part_mapped.txt", "w") as mutation_file: # Résumé des mutations de chaque read pour les reads partiellement mappés
	for name in range(len(name_part_mapped)):
		mutation_file.write(name_part_mapped[name]+" "+mutation_list[name]+"\n" + "\n")
mutation_file.close()

with open("summary.txt", "w") as summary_file: # Fichier résumé concernant le fichier .sam
	summary_file.write(str(header_storage) + "\n")
	summary_file.write("File : " + str(fichier) + "\n" + "\n")
	summary_file.write("Total reads :" + str(read_count) + "\n")
	summary_file.write("Total unmapped reads: " + str(unmapped_count) + "\n")
	summary_file.write("Total pairs with at least one unmapped read : " + str(int(len(seqlist_paired_unmapped)/2)) + "\n")
	summary_file.write("Total partially mapped reads: " + str(partially_mapped_count) + "\n")
	summary_file.write("Total pairs with at least one partially mapped read : " + str(int(len(seqlist_paired_part_mapped)/2)) + "\n")
summary_file.close()

with open("summary_data.txt", "w") as summary_file: # Fichier résumé concernant le fichier .sam
	summary_file.write("File " + str(fichier) + "\n")
	summary_file.write("Tot_reads " + str(read_count) + "\n")
	summary_file.write("Unmapped_reads " + str(unmapped_count) + "\n")
	summary_file.write("Pairs_with_one_unmapped_read " + str(int(len(seqlist_paired_unmapped)/2)) + "\n")
	summary_file.write("Part_mapped_reads " + str(partially_mapped_count) + "\n")
	summary_file.write("Pairs_with_one_part_mapped_read " + str(int(len(seqlist_paired_part_mapped)/2)) + "\n")
summary_file.close()

with open('reads_only_unmapped.fasta', 'w') as u: # Création du fichier fasta où on récupère tous les reads non mappés
	for seq in range(len(seqlist_unmapped)):
		u.write("> "+name_unmapped[seq]+'\n'+seqlist_unmapped[seq]+'\n') 
u.close()

with open('pair_unmapped.fasta', 'w') as u: # Création du fichier fasta où on récupère toutes les paires où au moins un read n'est pas mappé
	for seq in range(len(seqlist_paired_unmapped)):
		u.write("> "+name_paired_unmapped[seq]+'\n'+seqlist_paired_unmapped[seq]+'\n') 
u.close()

with open('reads_part_mapped.fasta', 'w') as u: # Création du fichier fasta où on récupère tous les reads partiellement mappés
	for seq in range(len(seqlist_part_mapped)):
		u.write("> "+name_part_mapped[seq]+'\n'+seqlist_part_mapped[seq]+'\n') 
u.close()

with open('pair_part_mapped.fasta', 'w') as u: # Création du fichier fasta où on récupère toutes les paires où au moins un read est partiellement mappé
	for seq in range(len(seqlist_paired_part_mapped)):
		u.write("> "+name_paired_part_mapped[seq]+'\n'+seqlist_paired_part_mapped[seq]+'\n') 
u.close()

print ("Results extracted." + "\n" + "\n" + "Success !")

f.close()
